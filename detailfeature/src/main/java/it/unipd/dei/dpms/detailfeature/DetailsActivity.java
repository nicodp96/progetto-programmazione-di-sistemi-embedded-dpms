package it.unipd.dei.dpms.detailfeature;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static com.google.android.instantapps.InstantApps.isInstantApp;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = "Details Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //caricamento toolbar e setup del back button
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        else {
            Log.d(TAG,"Impossibile caricare correttamente la toolbar");
        }

        //accetta intent in entrata
        getIncomingIntent();
    }

    private void getIncomingIntent(){
        Log.d(TAG, "getIncomingIntent: sta cercando nuovi intent");

        //Se l'intent possiede tutte le informazioni da caricare
        if ((getIntent().hasExtra("sku"))
                && (getIntent().hasExtra("nome"))
                && (getIntent().hasExtra("prezzo"))
                && (getIntent().hasExtra("colore"))
                && (getIntent().hasExtra("descrizione"))
                && (getIntent().hasExtra("immagine"))) {

            Log.d(TAG, "getIncomingIntent: sono stati trovati degli extra");

            //prendo tutto quello che mi serve sapere per mostrare la schermata dei dettagli
            String sku = getIntent().getStringExtra("sku");
            String nome = getIntent().getStringExtra("nome");
            Integer prezzo = getIntent().getIntExtra("prezzo", 0);
            String colore = getIntent().getStringExtra("colore");
            String descrizione = getIntent().getStringExtra("descrizione");

            //carica l'immagine
            Integer immagine = getIntent().getIntExtra("immagine", 0);

            //e crea la vista
            setView(sku, nome, prezzo, colore, descrizione, immagine);
        }
        if(getIntent().getExtras()==null){
            //se l'intent che arriva non ha extra

            //serve per evitare il bug della instant che carica una detail vuota
            //la prima volta che si clicca su un elemento

            finish();
        }
    }

    private void setView(String sku, String nome, Integer prezzo,
                         String colore, String descrizione,
                         Integer immagine){

        Log.d(TAG, "setView: setta l'immagine e le textViews.");
        //serve per passarlo nel metodo onClick
        final String skuCode=sku;

        //impostare le textView, la ImageView e il tasto like
        TextView vSku = findViewById(R.id.skud);
        vSku.setText(sku);
        TextView vNome = findViewById(R.id.nomed);
        vNome.setText(nome);
        TextView vPrezzo = findViewById(R.id.prezzod);
        String prezzoString=prezzo.toString()+",00€";
        vPrezzo.setText(prezzoString);
        TextView vColore = findViewById(R.id.colored);
        vColore.setText(colore);
        TextView vDescrizione = findViewById(R.id.descrizioned);
        vDescrizione.setText(descrizione);

        ImageView vImmagine =  findViewById(R.id.immagine);
        vImmagine.setImageResource(immagine);

        ImageButton like = findViewById(R.id.heart);

        //impostare il listener per il tasto
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Click listener cuore");

                Context context = getApplicationContext();

                if(isInstantApp(context)){
                    Log.d(TAG,"E' Instant");

                    //Si manda un AlertDialog che avvisa di installare l'applicazione
                    String title = "Attenzione";
                    String message = "Se si vuole accedere a questa funzione scaricare l'applicazione dal Play Store";
                    AlertDialog alertDialog = new AlertDialog.Builder(DetailsActivity.this).create();
                    alertDialog.setCancelable(true);
                    alertDialog.setTitle(title);
                    alertDialog.setMessage(message);
                    alertDialog.show();
                }
                else{
                    Log.d(TAG,"E' app");

                    //Con un intent si passa alla FavoriteListActivity

                    final Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://favoritemodule.dpms.dei.unipd.it/favorite"));

                    intent.setPackage(DetailsActivity.this.getPackageName());
                    intent.putExtra("sku",skuCode);

                    //richiede un risultato dalla FavoriteListActivity
                    startActivityForResult(intent,1);

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        //Invia notifiche diverse all'utente in base al risultato ricevute

        if(requestCode==1){
            if(resultCode== Activity.RESULT_OK){
                //Se il prodotto non è presente nella favorite

                Toast.makeText(this,"Prodotto inserito nei preferiti",Toast.LENGTH_SHORT).show();
            }
            else if(resultCode==Activity.RESULT_CANCELED){
                //Se il prodotto è presente nella favorite

                Toast.makeText(this,"Prodotto già inserito nei preferiti",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //setup del bottone di navigazione

        onBackPressed();
        return true;
    }

}


