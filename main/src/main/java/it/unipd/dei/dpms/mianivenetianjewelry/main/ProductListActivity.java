package it.unipd.dei.dpms.mianivenetianjewelry.main;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import it.unipd.dei.dpms.base.Product;
import static com.google.android.instantapps.InstantApps.isInstantApp;

public class ProductListActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    //variabili necessarie per il caricamento del database
    public static final List<Product> productList = new ArrayList<>();
    public static final List<Product> listaCollane = new ArrayList<>();
    public static final List<Product> listaBracciali = new ArrayList<>();
    public static final List<Product> listaOrecchini = new ArrayList<>();

    private static final String TABLE_NAME = "catalogo";
    private static final String DATABASE_NAME = "catalogointero.db";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        //caricamento toolbar
        Toolbar toolbar =  findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        //svuotamento della lista prodotti
        productList.clear();

        //caricamento database
        loadDataBase();

        //suddivisione dei prodotti nei fragments corrispondenti alla categoria
        setUpFragment();

        //caricamento del fragment di default
        loadFragment(new CollaneFragment());

        //collegamento del listener alla bottom navigation view
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        if(isInstantApp(this)) {
            instantBugFix();
        }
    }

    private void loadDataBase() {
        //caricamento database

        SQLiteDatabase db = null;

        File dbFile = this.getDatabasePath(DATABASE_NAME);

        try {
            if (!dbFile.getParentFile().exists() && !dbFile.getParentFile().mkdir()) {
                throw new IOException("Impossibile creare la cartella databases");
            }
            copyDataBase(dbFile);   //metodo dichiarato in seguito
            db = SQLiteDatabase.openDatabase(String.valueOf(dbFile),null, SQLiteDatabase.OPEN_READONLY);
        }catch(IOException e) {

            String TAG = "Main Activity";
            Log.d(TAG,"Il database non è stato copiato correttamente");
        }

        Cursor res = null;
        if (db != null) {
            res = db.rawQuery("select * from " + TABLE_NAME,null);
        }
        else{
            String TAG = "Main Activity";
            Log.d(TAG,"Il database non è stato copiato correttamente");
        }

        if (res!=null && (res.getCount())!=0 ) {
            //verifico che res non sia null e che vi sia stato salvato qualcosa

            while (res.moveToNext()) {
                //va esaminata ogni tupla per estrarre il valore di ogni colonna così da
                //creare un oggetto Product per la creazione della Lista

                createProduct(res);

            }
            res.close();
        }
        else{
            //se qualcosa è andato storto si avvisa l'utente che il caricamento del database non è avvenuto

            String title = "Attenzione";
            String message = "Errore nel caricamento del database";
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.show();
        }

    }

    private void copyDataBase(File dbFile) throws IOException {
        //apre il database locale come input stream

        InputStream myInput = this.getAssets().open("databases/"+DATABASE_NAME);

        //percorso al database vuoto appena creato
        String outFileName = String.valueOf(dbFile);
        OutputStream myOutput = new FileOutputStream(outFileName);

        //trasferisco i byte da input file a output file
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //chiudo gli stream
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private void createProduct(Cursor res){
        //metodo per creare i singoli prodotti a partire dal contenuto del database

        Product product;

        String sku = res.getString(0);
        String nome = res.getString(1);
        String categoria = res.getString(2);
        Integer prezzo = res.getInt(3);
        String colore = res.getString(5);
        String descrizione = res.getString(6);

        //estrae il nome dell'immagine dal database
        String nome_immagine = res.getString(4);

        //ricava l'ID della risorsa
        Integer IDimmagine = getResources().getIdentifier(nome_immagine, "mipmap", getPackageName());

        product = new Product(sku, nome, categoria, prezzo, IDimmagine, colore, descrizione);

        //aggiunta oggetto alla lista
        productList.add(product);
    }

    private void setUpFragment(){
        //metodo per organizzare i prodotti nei vari fragments in base alla categoria

        //svuotamento liste per i fragment
        listaCollane.clear();
        listaBracciali.clear();
        listaOrecchini.clear();

        //seleziona gli elementi da mettere delle liste di ciascuna categoria
        for(int i=0; i<productList.size();i++){
            switch (productList.get(i).getCategoria()) {
                case "Collana":
                    listaCollane.add(productList.get(i));
                    break;
                case "Bracciale":
                    listaBracciali.add(productList.get(i));
                    break;
                case "Orecchini":
                    listaOrecchini.add(productList.get(i));
            }
        }
    }

    private boolean loadFragment(Fragment fragment) {
        //caricamento dei fragments

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    private void instantBugFix(){
        //manda un intent vuoto in modo da evitare il bug presente all'apertura della detail
        //del primo elemento che viene selezionato

        //viene usato solo nella instant

        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://detailfeature.dpms.dei.unipd.it/details"));
        i.setPackage(getPackageName());
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // aggiunta degli elementi  alla toolbar

        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //comportamento quando viene cliccato un bottone nella toolbar

        if (item.getItemId() == R.id.action_favorite) {
            //comportamento quando viene cliccato il bottone "preferiti"

            if (isInstantApp(this)) {
                //comportamento in modalità Instant: AlertDialog

                String title = "Attenzione";
                String message = "Se si vuole accedere a questa funzione scaricare l'applicazione dal Play Store";
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setTitle(title);
                builder.setMessage(message);
                builder.show();
            } else {
                //se è una app si può far partire la favoriteListActivity

                Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://favoritemodule.dpms.dei.unipd.it/favorite"));

                intent.setPackage(ProductListActivity.this.getPackageName());
                startActivity(intent);
            }
            return true;
        }
        else
            //l'azione dell'utente non è stata riconosciuta: invoco la superclasse

            return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //metodo per permettere la navigazione tra i fragments

        Fragment fragment = null;

        if(item.getItemId()==R.id.category_collane) {
            fragment = new CollaneFragment();
        }
        else if (item.getItemId()==R.id.category_bracciali){
            fragment = new BraccialiFragment();
        }
        else if(item.getItemId()==R.id.category_orecchini){
            fragment = new OrecchiniFragment();
        }
        return loadFragment(fragment);
    }

    //lo stato è salvato dal super
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //ripristino lo stato dei fragments

        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

}
