package it.unipd.dei.dpms.mianivenetianjewelry.main;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class BraccialiFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //Carico la vista relativa al fragment
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_bracciali, null);

        //imposto la RecyclerView caricando l'Adapter mAdapter
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        ProductAdapter mAdapter = new ProductAdapter(getContext(), ProductListActivity.listaBracciali);

        if (getActivity() != null) {
            //creo il layout
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);

            //imposto ItemAnimator per gestire le animazioni della RecyclerView
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            //inserisco separatori tra le tuple
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        } else {

            String TAG = "BraccialiFragment";
            Log.d(TAG,"Impossibile ricavare il context");
        }

        recyclerView.setAdapter(mAdapter);

        return view;
    }
}