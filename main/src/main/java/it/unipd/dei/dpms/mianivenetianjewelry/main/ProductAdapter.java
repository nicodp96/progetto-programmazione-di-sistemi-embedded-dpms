package it.unipd.dei.dpms.mianivenetianjewelry.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import it.unipd.dei.dpms.base.Product;

import static android.content.ContentValues.TAG;


//classe per definire le recyclerView e i relativi prodotti

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private final List<Product> productList;
    private final Context context;

    ProductAdapter(Context context, List<Product> productList) {
        //costruttore a cui viene passata la lista di prodotti

        this.context = context;
        this.productList = productList;
    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        //singolo componente che verrà inserito dentro alla RecyclerView

        final TextView sku;
        final TextView nome;
        final TextView prezzo;
        final ImageView immagine;
        final RelativeLayout relativeLayout;

        MyViewHolder(View view) {
            super(view);

            sku =  view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.sku);
            nome =  view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.nome);

            prezzo =  view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.prezzo);
            immagine =  view.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.immagine);

            //serve per cliccare sull'elemento
            relativeLayout = itemView.findViewById(it.unipd.dei.dpms.mianivenetianjewelry.main.R.id.relative_layout);
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(it.unipd.dei.dpms.mianivenetianjewelry.main.R.layout.product_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //ottengo gli elementi dal dataset in questa posizione
        //rimpiazzo il contenuto della view con quell'elemento

        final int pos = holder.getAdapterPosition();
        Product product = productList.get(pos);

        Log.d(TAG, "holder at position " + pos + " is " + holder);

        holder.sku.setText(product.getSku());
        holder.nome.setText(product.getNome());

        String prezzoString = Integer.toString(product.getPrezzo())+",00€";
        holder.prezzo.setText(prezzoString);

        //imposta ogni volta la risorsa in base al nome(=id della risorsa) salvato in prodotto
        holder.immagine.setImageResource(product.getIDimage());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Apertura dettagli");

                Intent intent = getIntentWithDetails(context,pos,productList);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    private static Intent getIntentWithDetails(Context context, int position, List<Product> productList){
        
        final Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://detailfeature.dpms.dei.unipd.it/details"));

        //aggiungo all'intent le informazioni che mi serviranno
        intent.setPackage(context.getPackageName());
        intent.putExtra("sku",productList.get(position).getSku());
        intent.putExtra("nome",productList.get(position).getNome());
        intent.putExtra("prezzo",productList.get(position).getPrezzo());
        intent.putExtra("colore",productList.get(position).getColore());
        intent.putExtra("descrizione",productList.get(position).getDescrizione());
        intent.putExtra("immagine",productList.get(position).getIDimage());

        return intent;
    }
}
