package it.unipd.dei.dpms.favoritefeature;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

// Classe che gestisce il database e si occupa della creazione dello stesso
@Database(entities = {FavoriteProduct.class}, version = 1)
abstract class FavoriteProductDatabase extends RoomDatabase{

    private static FavoriteProductDatabase INSTANCE;

    //Definisce il DAO che lavora con il database
    abstract FavoriteProductDao favoriteProductDao();

    static FavoriteProductDatabase getDatabase(Context context){
       if(INSTANCE==null){
           INSTANCE=Room.databaseBuilder(context.getApplicationContext(),
                   FavoriteProductDatabase.class,
                   "favorite_product_database")
                   .allowMainThreadQueries() //permette di eseguire queries nel main thread
                   .build();
       }
       return INSTANCE;
   }
}
