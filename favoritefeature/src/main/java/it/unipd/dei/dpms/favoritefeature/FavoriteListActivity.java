package it.unipd.dei.dpms.favoritefeature;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import it.unipd.dei.dpms.mianivenetianjewelry.main.ProductListActivity;

public class FavoriteListActivity extends AppCompatActivity {

    private static final String TAG = "Favorite Activity";

    //serve per mandare indietro una risposta alla detailsActivity
    private final Intent returnIntent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);

        //caricamento toolbar e setup del back button
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        else {
            Log.d(TAG,"Impossibile caricare correttamente la toolbar");
        }

        //creazione collegamento al database
        FavoriteProductDatabase fav_db = FavoriteProductDatabase.getDatabase(this);

        //l'activity può essere avviata o con un intent (è stato schiacciato il cuore nella DetailsActivity)
        //oppure mediante il bottone cuore nella toolbar

        //con getIntent() ottiene l'intent anche se a mandarlo è il link del cuore
        //in questo caso carica semplicemente l'activity

        String sku;
        if (getIntent().hasExtra("sku")){
            Log.d(TAG,"Intent arrivato con degli extra");
            sku = getIntent().getStringExtra("sku");

            if(fav_db.favoriteProductDao().isPresent(sku)!=null){
                //significa che l'elemento è già presente
                Log.d(TAG,"Sku presente, si notifica l'utente");

                //manda indietro una risposta in modo che l'utente sappia che è già presente
                setResult(RESULT_CANCELED,returnIntent);
                finish();
            }
            else if((fav_db.favoriteProductDao().isPresent(sku))==null){
                //Se è null viene inserito
                Log.d(TAG,"Sku non presente, viene aggiunto un prodotto");

                //si recuperano i dati caricati dal database principale per poi inserirli nel database dei preferiti
                for(int i = 0; i< ProductListActivity.productList.size(); i++){
                    String skudb = ProductListActivity.productList.get(i).getSku();
                    if(skudb.equals(sku)){
                        String nome = ProductListActivity.productList.get(i).getNome();
                        Integer prezzo = ProductListActivity.productList.get(i).getPrezzo();
                        Integer immagine = ProductListActivity.productList.get(i).getIDimage();

                        insertFavProduct(fav_db, sku, nome, prezzo, immagine);

                        break;
                    }
                }
                //manda indietro una risposta avvisando che il prodotto è stato inserito
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        }
        if(getIntent().hasExtra("deleteSku")){
            Log.d(TAG,"Intent deleteSku arrivato");

            sku = getIntent().getStringExtra("deleteSku");

            //cancellazione elemento
            fav_db.favoriteProductDao().deleteSku(sku);

            //toast di conferma
            Toast.makeText(this,"Elemento eliminato",Toast.LENGTH_SHORT).show();

            //elimino l'animazione dell'apertura dell'activity
            this.overridePendingTransition(0,0);
        }

        //caricamento in favoriteList di tutti i prodotti contenuti nel database
        List<FavoriteProduct> favProductList = fav_db.favoriteProductDao().getFavoriteProducts();

        //caricamento view
        RecyclerView fRecyclerView = findViewById(R.id.recycler_view);
        FavoriteProductAdapter fAdapter = new FavoriteProductAdapter(this, favProductList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(FavoriteListActivity.this.getApplicationContext());
        fRecyclerView.setLayoutManager(mLayoutManager);

        //imposta ItemAnimator e ItemDecoration
        fRecyclerView.setItemAnimator(new DefaultItemAnimator());
        fRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        fRecyclerView.setAdapter(fAdapter);
    }


    @Override
    public boolean onSupportNavigateUp() {
        //setup del bottone di navigazione

        onBackPressed();
        return true;
    }


    private static void insertFavProduct(final FavoriteProductDatabase db, String sku, String nome, Integer prezzo, Integer immagine){
        //metodo utilizzato per caricare gli elementi nel database

        FavoriteProduct fav = new FavoriteProduct();
        fav.setSku(sku);
        fav.setNome(nome);
        fav.setPrezzo(prezzo);
        fav.setImmagine(immagine);

        db.favoriteProductDao().insert(fav);

    }
}
