package it.unipd.dei.dpms.favoritefeature;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


/*Classe che rappresenta l'unica tabella del database, denominata "favoriteProduct", ha un'unica
  colonna "sku", contenente l'sku del prodotto aggiunto ai preferiti */

@Entity(tableName = "favoriteProduct")
public class FavoriteProduct {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "sku")
    private String sku = "";

    @NonNull
    @ColumnInfo(name = "nome")
    private String nome = "";

    @NonNull
    @ColumnInfo(name = "prezzo")
    private Integer prezzo = 0;

    @NonNull
    @ColumnInfo(name = "immagine")
    private Integer immagine = 0;

    @NonNull
    public String getSku() {
        return sku;
    }
    public void setSku(@NonNull String sku) {
        this.sku = sku;
    }
    @NonNull
    public String getNome() { return nome; }
    public void setNome(@NonNull String nome) { this.nome = nome; }
    @NonNull
    public Integer getPrezzo() { return prezzo; }
    public void setPrezzo(@NonNull Integer prezzo) { this.prezzo = prezzo; }
    @NonNull
    public Integer getImmagine() { return immagine; }
    public void setImmagine(@NonNull Integer immagine) { this.immagine = immagine; }


}
