package it.unipd.dei.dpms.favoritefeature;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import static android.content.ContentValues.TAG;

import static it.unipd.dei.dpms.mianivenetianjewelry.main.ProductListActivity.productList;

//classe per collegare i dati relativi ai prodotti preferiti alla activity che li visualizza

public class FavoriteProductAdapter extends RecyclerView.Adapter<FavoriteProductAdapter.MyViewHolder> {

    private final List<FavoriteProduct> favoriteProductList;
    private final Context context;

     FavoriteProductAdapter(Context context, List<FavoriteProduct> favoriteProductList) {
         //costruttore a cui viene passata la lista di prodotti

         this.favoriteProductList = favoriteProductList;
         this.context = context;
     }

    class MyViewHolder extends RecyclerView.ViewHolder {
        //singolo componente che verrà inserito dentro alla RecyclerView

        final TextView sku;
        final TextView nome;
        final TextView prezzo;
        final ImageView immagine;
        final RelativeLayout relativeLayout;
        final ImageButton cestino;

        MyViewHolder(View view) {
            super(view);
            sku =  view.findViewById(it.unipd.dei.dpms.favoritefeature.R.id.skuf);
            nome =  view.findViewById(R.id.nomef);
            prezzo =  view.findViewById(R.id.prezzof);
            immagine =  view.findViewById(it.unipd.dei.dpms.favoritefeature.R.id.thumbnailf);

            //serve per cancellare gli elementi
            cestino =  view.findViewById(R.id.cestino);

            //serve per poter cliccare su un elemento
            relativeLayout = itemView.findViewById(R.id.relative_layout_fav);
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //ottengo gli elementi dal dataset in questa posizione
        //rimpiazzo il contenuto della view con quell'elemento

        final int pos = holder.getAdapterPosition();
        final FavoriteProduct favoriteProduct = favoriteProductList.get(pos);

        Log.d(TAG, "holder at position " + pos + " is " + holder);

        holder.sku.setText(favoriteProduct.getSku());
        holder.nome.setText(favoriteProduct.getNome());

        String prezzoString = favoriteProduct.getPrezzo()+",00€";
        holder.prezzo.setText(prezzoString);

        holder.immagine.setImageResource(favoriteProduct.getImmagine());

        holder.cestino.setOnClickListener(new View.OnClickListener() {
            //componente cestino per poter eliminare elementi dalla lista preferiti

            @Override
            public void onClick(View v) {
                Log.i(TAG,"Cestino cliccato");

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://favoritemodule.dpms.dei.unipd.it/favorite"));

                intent.setPackage(context.getPackageName());

                //basta sapere l'sku per identificare l'elemento da cancellare
                intent.putExtra("deleteSku",favoriteProductList.get(pos).getSku());

                context.startActivity(intent);

                //chiude la vecchia activity per caricarne una nuova aggiornata
                ((Activity) context).finish();
            }
        });

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG,"Apertura dettagli");

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://detailfeature.dpms.dei.unipd.it/details"));

                intent.setPackage(context.getPackageName());
                intent.putExtra("sku",favoriteProductList.get(pos).getSku());
                intent.putExtra("nome",favoriteProductList.get(pos).getNome());
                intent.putExtra("prezzo",favoriteProductList.get(pos).getPrezzo());
                intent.putExtra("immagine",favoriteProductList.get(pos).getImmagine());

                //mancano i dettagli che sono presenti nel database originale
                for(int i=0; i<productList.size();i++){
                    //itero in tutti i prodotti appartenenti al database (contenuti in productList)

                    if((favoriteProductList.get(pos).getSku()).equals(productList.get(i).getSku())){
                        //recupero colore e descrizione confrontando l'sku
                        intent.putExtra("colore",productList.get(i).getColore());
                        intent.putExtra("descrizione",productList.get(i).getDescrizione());
                    }
                }

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
         return favoriteProductList.size();
     }
}