package it.unipd.dei.dpms.favoritefeature;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

// Interfaccia che permette di definire le query necessarie per l'utilizzo del database
@Dao
interface FavoriteProductDao {

    @Insert
    void insert(FavoriteProduct product);

    //per cancellare una sola tupla
    @Query("DELETE FROM favoriteProduct WHERE sku = :sku")
    void deleteSku(String sku);

    //per selezionare tutti i dati
    @Query("SELECT * FROM favoriteProduct")
    List<FavoriteProduct> getFavoriteProducts();

    //seleziona l'elemento con sku = sku inserito
    @Query ("SELECT * FROM favoriteProduct WHERE sku = :sku ")
    FavoriteProduct isPresent(String sku);
}
